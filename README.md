# Accumulate Networks

## MainNet

- mainnet.accumulatenetwork.io
- Executor: v1
- API: v2

## Kermit

For testing applications. Accounts and data will be preserved if possible.

- kermit.accumulatenetwork.io
- Executor: v1
- API: v2, v3

## Fozzie

For testing new core releases. Accounts and data may be wiped.

- fozzie.accumulatenetwork.io
- Executor: v1
- API: v2, v3

## Network Map ([networkmap.yaml](networkmap.yaml))

- Provides details of Accumulate Pubic Networks
- Current software version (tags)
- Chain services & their root hash.

## P2P bootstrap nodes

- /dns/bootstrap.accumulate.defidevs.io/tcp/16593/p2p/12D3KooWGJTh4aeF7bFnwo9sAYRujCkuVU1Cq8wNeTNGpFgZgXdg

## Defunct Networks

- Beta TestNet (beta.testnet.accumulatenetwork.io)
